<?php

namespace Setdroy\MallPayU\Classes;

use Dompdf\Exception;
use OFFLINE\Mall\Classes\Payments\PaymentProvider;
use OFFLINE\Mall\Models\Order;
use OFFLINE\Mall\Models\PaymentGatewaySettings;
use OFFLINE\Mall\Classes\Payments\PaymentResult;
use Setdroy\MallPayU\Classes\PaymentState\PaymentVerificationState;
use OFFLINE\Mall\Classes\PaymentState\PaidState;
use OFFLINE\Mall\Classes\PaymentState\FailedState;

use OpenPayU_Configuration;
use OpenPayU_Order;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Session;
use Setdroy\MallPayU\Classes\Payments\PayUPaymentResult;
use October\Rain\Support\Facades\Url;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Lang;

class PayUProvider extends PaymentProvider
{

    /**
     * {@inheritdoc}
     */
    public function name(): string
    {
        return 'PayU';
    }

    /**
     * {@inheritdoc}
     */
    public function identifier(): string
    {
        return 'payu-rest';
    }

    /**
     * {@inheritdoc}
     */
    public function validate(): bool
    {
        return true;
    }

    protected function init(){
        if (PaymentGatewaySettings::get('payu_test_mode')) {
            OpenPayU_Configuration::setEnvironment('sandbox');
        } else {
            OpenPayU_Configuration::setEnvironment('secure');
        }

        //set POS ID and Second MD5 Key (from merchant admin panel)
        OpenPayU_Configuration::setMerchantPosId(PaymentGatewaySettings::get('payu_pos_id'));
        OpenPayU_Configuration::setSignatureKey(decrypt(PaymentGatewaySettings::get('payu_second_key')));

        //set Oauth Client Id and Oauth Client Secret (from merchant admin panel)
        OpenPayU_Configuration::setOauthClientId(PaymentGatewaySettings::get('payu_oauth_client_id'));
        OpenPayU_Configuration::setOauthClientSecret(decrypt(PaymentGatewaySettings::get('payu_oauth_secret')));
    }


    /**
     * {@inheritdoc}
     */
    public function process(PaymentResult $result): PaymentResult
    {
        if($this->isOrderInPaymentVerification()){
            return $result->fail([], 'Order is in payment verification, you need to wait for order payment will be verified');
        }

        try {
            $transaction = $this->createPaymentOrder();

            $this->order->payment_transaction_id = $transaction->orderId;
            $this->order->save();
        } catch (Throwable $e) {
            return $result->fail([], $e);
        }

        Session::put('mall.payment.callback', self::class);
        Session::put('mall.payu.transactionReference', $transaction->orderId);

        return $result->redirect($transaction->redirectUri);
    }

    private function isOrderInPaymentVerification(){
        return $this->order->payment_state === PaymentVerificationState::class;
    }

    protected function createPaymentOrder()
    {
        $this->init();

        $order = [];
        $order['continueUrl'] = $this->returnUrl();
        $order['notifyUrl'] = $this->notifyUrl();
        $order['customerIp'] = $_SERVER['REMOTE_ADDR'];
        $order['merchantPosId'] = OpenPayU_Configuration::getMerchantPosId();
        $order['description'] = Lang::get('setdroy.mallpayu::lang.order_number').': #' . $this->order->order_number;
        $order['currencyCode'] = $this->order->currency['code'];
        $order['totalAmount'] = $this->order->total_in_currency * 100;
        $order['extOrderId'] = $this->order->payment_hash.'|'.time(); //must be unique!

        foreach ($this->order->products as $productKey => $product) {
            $order['products'][$productKey]['name'] = $product->name;
            $order['products'][$productKey]['unitPrice'] = $product->price_post_taxes;
            $order['products'][$productKey]['quantity'] = $product->quantity;
        }

        //optional section buyer
        $order['buyer']['email'] = $this->order->customer->user->email;
        $order['buyer']['firstName'] = $this->order->customer->firstname;
        $order['buyer']['lastName'] = $this->order->customer->lastname;

        if($this->order->payment_transaction_id !== null && $this->cancelPaymentOrder($this->order->payment_transaction_id)){
            LOG::info('REMOVED OLD ORDER: '.$this->order->payment_transaction_id );
        }

        Log::info('NEW ORDER PAYU PAYMENT', $order);

        $transaction = OpenPayU_Order::create($order);

        if ($transaction->getStatus() !== OpenPayU_Order::SUCCESS) {
            return false;
        }
        return $transaction->getResponse();
    }

    private function cancelPaymentOrder($orderId){
        $this->init();
        Log::info('CANCEL ORDER PAYU PAYMENT: '. $orderId);
        $response = OpenPayU_Order::cancel($orderId);
        return $response->getStatus() === OpenPayU_Order::SUCCESS;
    }

    protected function getPaymentOrder($orderId)
    {
        $this->init();
        $transaction = OpenPayU_Order::retrieve($orderId);
        if ($transaction->getStatus() === OpenPayU_Order::SUCCESS) {
            return $transaction->getResponse()->orders[0];
        }
        return false;
    }

    /**
     * PayPal has processed the payment and redirected the user back.
     *
     * @param PaymentResult $result
     *
     * @return PaymentResult
     */
    public function complete(PaymentResult $result): PayUPaymentResult
    {
        $payUResult = new PayUPaymentResult($result->provider, $result->order);

        if(isset($payUResult->order) && $payUResult->order !== null){
            $order = $payUResult->order;
        }else{
            return $payUResult->fail([], []);
        }

        $transaction = $this->getPaymentOrder($order->payment_transaction_id);

        return $payUResult->verification((array) $transaction, []);
    }


    /**
     * {@inheritdoc}
     */
    public function settings(): array
    {
        return [
            'payu_test_mode' => [
                'label' => 'setdroy.mallpayu::lang.test_mode',
                'comment' => 'setdroy.mallpayu::lang.test_mode_comment',
                'span' => 'left',
                'type' => 'switch',
            ],
            'payu_pos_id' => [
                'label' => 'setdroy.mallpayu::lang.pos_id',
                'comment' => 'setdroy.mallpayu::lang.pos_id_comment',
                'span' => 'left',
                'type' => 'text',
            ],
            'payu_second_key' => [
                'label' => 'setdroy.mallpayu::lang.second_key',
                'comment' => 'setdroy.mallpayu::lang.second_key_comment',
                'span' => 'left',
                'type' => 'text',
            ],
            'payu_oauth_client_id' => [
                'label' => 'setdroy.mallpayu::lang.oauth_client_id',
                'comment' => 'setdroy.mallpayu::lang.oauth_client_id_comment',
                'span' => 'left',
                'type' => 'text',
            ],
            'payu_oauth_secret' => [
                'label' => 'setdroy.mallpayu::lang.oauth_secret',
                'comment' => 'setdroy.mallpayu::lang.oauth_secret_comment',
                'span' => 'left',
                'type' => 'text',
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function encryptedSettings(): array
    {
        return ['payu_second_key', 'payu_oauth_secret'];
    }

    private function paymentId()
    {
        return Session::get('mall.payment.id');
    }

    public function notifyUrl(): string
    {
        return env('MALL_PAYU_NOTIFY_URL', Url::to('oc_mall_payu_callback'));
    }

    public function notify(){
        $input = Input::all();

        Log::info('PAYU CALLBACK: ', $input);
        Log::info('PAYU CALLBACK STATUS: '. isset($input['order']['status']) ? $input['order']['status'] : 'NULL');

        try{
            $order = Order::where('payment_transaction_id', $input['order']['orderId'])->first();

            if($order){
                if($input['order']['status'] === 'COMPLETED'){
                    $order->payment_state = PaidState::class;
                }

                if($input['order']['status'] === 'CANCELED'){
                    $order->payment_state = FailedState::class;
                }

                if($input['order']['status'] === 'REJECTED'){
                    $order->payment_state = FailedState::class;
                }

                $order->save();
            }

        }catch (Exception $e){
            Log::error('PAYU NOTIFY CALLBACK ERROR : ', $e);
        }
    }
}