<?php

namespace Setdroy\MallPayU\Classes\Payments;

use OFFLINE\Mall\Classes\Payments\PaymentResult;
use Setdroy\MallPayU\Classes\PaymentState\PaymentVerificationState;

/**
 * The PaymentResult contains the result of a payment attempt.
 */
class PayUPaymentResult extends PaymentResult
{

    /**
     * The payment was successful.
     *
     * The payment is logged, associated with the order
     * and the order is marked as payment verification.
     *
     * @param array $data
     * @param       $response
     *
     * @return PaymentResult
     */
    public function verification(array $data, $response): self
    {
        $this->successful = true;

        try {
            $payment = $this->logSuccessfulPayment($data, $response);
        } catch (\Throwable $e) {
            // Even if the log failed we *have* to mark this order as paid since the payment went already through.
            logger()->error(
                'Setdroy.MallPayU: Could not log verification payment.',
                ['data' => $data, 'response' => $response, 'order' => $this->order, 'exception' => $e]
            );
        }

        try {
            $this->order->payment_id    = $payment->id;
            $this->order->payment_state = PaymentVerificationState::class;
            $this->order->save();
        } catch (\Throwable $e) {
            // If the order could not be marked as paid the shop admin will have to do this manually.
            logger()->critical(
                'Setdroy.MallPayU: Could not mark paid order as paid.',
                ['data' => $data, 'response' => $response, 'order' => $this->order, 'exception' => $e]
            );
        }

        return $this;
    }
}
