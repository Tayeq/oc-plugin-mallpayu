<?php

namespace Setdroy\MallPayU\Classes\PaymentState;

use OFFLINE\Mall\Classes\PaymentState\FailedState;
use OFFLINE\Mall\Classes\PaymentState\PaidState;
use OFFLINE\Mall\Classes\PaymentState\PendingState;

class PaymentVerificationState
{
    public static function getAvailableTransitions(): array
    {
        return [
            PendingState::class,
            FailedState::class,
            PaidState::class,
        ];
    }

    public static function label(): string
    {
        $parts = explode('\\', get_called_class());
        $state = snake_case($parts[count($parts) - 1]);

        return trans('setdroy.mallpayu::lang.order.payment_states.' . $state);
    }

    public static function color(): string
    {
        return '#cc0';
    }
}
