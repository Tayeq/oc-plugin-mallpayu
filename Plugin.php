<?php namespace Setdroy\MallPayU;

use Backend;
use System\Classes\PluginBase;
use Setdroy\MallPayU\Classes\PayUProvider;
use OFFLINE\Mall\Classes\Payments\PaymentGateway;
use OFFLINE\Mall\Models\Order;
use OFFLINE\Mall\Classes\PaymentState\PaidState;
use Setdroy\MallPayU\Classes\PaymentState\PaymentVerificationState;

/**
 * MallPayU Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */

    public $require = ['OFFLINE.Mall'];

    public function pluginDetails()
    {
        return [
            'name'        => 'MallPayU',
            'description' => 'PayU Payments for OFFLINE.MALL',
            'author'      => 'Setdroy',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {
        $gateway = $this->app->get(PaymentGateway::class);
        $gateway->registerProvider(new PayUProvider());

        Order::extend(function($model) {
            $model->addDynamicMethod('canBePaid', function() use ($model) {
                return ($model->payment_state !== PaidState::class && $model->payment_state !== PaymentVerificationState::class);
            });
        });

    }
}
