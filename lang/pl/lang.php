<?php
return [
    'test_mode' => 'Tryb testowy',
    'test_mode_comment' => 'Uruchamiaj płatności w trybie sandbox',
    'pos_id' => 'Id punktu płatności',
    'pos_id_comment' => '(pos_id)',
    'second_key' => 'Drugi klucz',
    'second_key_comment' => '(MD5)',
    'oauth_client_id' => 'OAuth Client ID',
    'oauth_client_id_comment' => '(client_id)',
    'oauth_secret' => 'OAuth Secret',
    'oauth_secret_comment' => '(client_secret)',
    'order_number' => 'Numer zamówienia',
    'order' => [
        'payment_states' => [
            'payment_verification_state' => 'Weryfikacja płatności'
        ]
    ]
];